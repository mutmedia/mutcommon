﻿using UnityEngine;

namespace MutCommon {
    public class App: MonoBehaviour {
        public void Quit() {
            Application.Quit();
        }
    }
}